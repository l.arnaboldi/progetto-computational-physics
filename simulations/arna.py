from make import *
import math

mean_a = [0.5, 2.5, 5.]
#diffusions = log(0.000005, 5, 100**0.33)

"""Zero isole"""
for ma in mean_a:
    for diff in diffusions:
        make("_noisland", [diff], [0.], ma, ma, 0.4, [])
        for w in [0.]+log(5e-2,5e2,6):
            if w*math.sqrt(diff*ma) < 0.001:
                continue
            make("_noislands", [diff], [w*math.sqrt(diff*ma)], ma, ma, 0.4, [])

"""Un isola: vedo come si deforma"""
for diff in diffusions:
    make("_oasis-normal", [diff], [0.], -0.5, 0.01, 0.0001, ["316 366 103 153 10."])
    make("_oasis-x", [diff], [0.], -0.5, 0.01, 0.0001, ["296 386 103 153 10."])
    make("_oasis-y", [diff], [0.], -0.5, 0.01, 0.0001, ["316 366 83 173 10."])
    make("_oasis-fascia", [diff], [0.], -0.5, 0.01, 0.0001, ["316 366 1 255 10."])
    for w in [0.]+log(5e-2,5e2,10):
        if w*math.sqrt(diff*0.5) < 0.001:
            continue
        make("_oasis-normal", [diff], [w*math.sqrt(diff*0.5)], -0.5, 0.01, 0.0001, ["316 366 103 153 10."])
        make("_oasis-x", [diff], [w*math.sqrt(diff*0.5)], -0.5, 0.01, 0.0001, ["296 386 103 153 10."])
        make("_oasis-y", [diff], [w*math.sqrt(diff*0.5)], -0.5, 0.01, 0.0001, ["316 366 83 173 10."])
        make("_oasis-fascia", [diff], [w*math.sqrt(diff*0.5)], -0.5, 0.01, 0.0001, ["316 366 1 255 10."])
