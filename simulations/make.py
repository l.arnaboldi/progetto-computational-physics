#!/usr/bin/env python
# -*- coding: utf-8 -*-

## a: average std_dev correlation
## b=1
## diffusion
## velocity
## sigma_width
## N_rectangles
## lato_lungo min max lato_corto min max offset

import os
from math import log as ll

def make(name, diffs, vels, a, astd, acorr, rectangles, b=1, sigma="0.05"):
	Len = len(diffs)*len(vels)
	counter = 0
	for diff in diffs:
		for vel in vels:
			counter = counter+1
			print(counter, "/", Len)
			path = name+"_D_"+format(diff, '.1e')+"_U_"+format(vel, '.3f')+"_a_"+str(a)+"_std_"+str(astd)+"_corr_"+str(acorr)
			os.mkdir(path)
			conf = open(path+"/config.txt", "w")
			parms = str(a)+" "+str(astd)+" "+str(acorr)+"\n"+str(b)
			print(parms)
			st = parms+"\n"+format(diff, '.6f')+"\n"+format(vel, '.3f')+"\n"+sigma+"\n"+str(len(rectangles))+"\n"
			for r in rectangles:
				st = st+r
			conf.write(st)
			conf.close()

def lin(start, end, step):# step > 0 !!!
	l = []
	val = start
	for i in range(round((end-start)/step)+1):
		l.append(val)
		val = val+step
	return l

def log(start, end, step):# step > 1 !!!
	l = []
	val = start
	for i in range(round(ll(end/start)/ll(step))+1):
		l.append(val)
		val = val*step
	return l

def test():
	print("0.1, 0.2, 0.3", lin(0.1, 0.3, 0.1))
	print("from E-6 to 1, step 10", log(0.000001, 1, 10))
	print("Creating 21 folders")
	make("Test", log(0.000001, 1, 10), lin(0.1, 0.3, 0.1), "", "")

# possible ranges
velocities = lin(0, 2.5, 0.15)
velocities.extend([3, 4, 6])
diffusions = log(0.000005, 5, 100**0.33)

## make dirs

#make("2strips", (0.00001,), (0.8,), 0.7, 0.2, 0.1, ("350 450 0 256 7.\n", "800 900 0 256 7.\n", "500 750 0 256 -0.5\n"))#corre abbastanza veloce da non morire?
