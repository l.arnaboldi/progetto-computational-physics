<!-- Copyright 2019 Luca Arnaboldi -->

<!DOCTYPE html>
<html lang="it">
<head>
  <title>Log Simulazione</title>
  <!--meta http-equiv="refresh" content="5"--> 
  <meta charset="UTF-8">
  <!-- Bootstrap CSS + JS (including Popper and jQuery-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <!-- FontAwesome -->
  <script src="https://kit.fontawesome.com/2d8a99623a.js"></script>
  <!--link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- Personal CSS -->
  <link rel="stylesheet"  href="{_static_}style.css" />
</head>
 <body>
 <!-- Header -->
<header>
  <div class="jumbotron jumbotron-fluid mb-0">
    <div class="container">
      <h1>Simulazione</h2>
      <h5>La simulazione sta girando su 131.114.73.188, il server del Mannella</h5>
    </div>
  </div>
</header>
<!--Contenuto -->
<div class="container">
  <div class="row">
    <table class="table">
    <tr><th>#</th><th>File  di log</th></tr>
    <?php
      $n = 1;
      foreach(glob("log/terminated/*") as $file_path) {
        $file = basename($file_path);
        echo('<tr><td>'.$n.'</td><td><a href="./log?f='.$file.'">'.$file."</a></td></tr>");
        $n = $n + 1;
      }
    ?>
    </table>
  </div>
  <div class="row">
  <h6> Simulazione in corso</h6>
  <div class="my-4 p-4 text-white bg-dark">
  <p style="font-family:'Lucida Console', monospace">
 <?php
   $filename = './log/logfile.txt';
   if($_SERVER['REQUEST_METHOD'] == 'GET') {
     $logfile = fopen($filename, 'r') or die("Can't open the file!");
     if ($logfile == false) {
       echo('Non riesco a leggere il file!');
     } else {
       while(!feof($logfile)) {
         echo fgets($logfile) . "<br>";
       }
       fclose($logfile);
     }
   } else {
     if ($_POST['reset'] == 'true') {
       if ($_POST['reset_mode'] == 'soft') {
         $logfile = fopen($filename, 'r');
         $fr = fgets($logfile);
         $name = str_replace(' Inizio la simulazione: ', '', $fr);
         fclose($logfile);
         $name = substr($name, 0, -1);
         copy($filename, './log/terminated/'.$name.'.log');
       } else {
         foreach(glob("log/terminated/*") as $file_path) {
           unlink($file_path);
         }
       }
       $logfile = fopen($filename, 'w');
       if ($logfile == false) {
         echo('Non riesco a leggere il file!');
       } else {
         fclose($logfile);
         echo('Reset: done!<br>');
       }
     } elseif ($_POST['reset'] == 'false') {
       $logfile = fopen($filename, 'a');
       if ($logfile == false) {
         echo('Non riesco a leggere il file!');
       } else {
         if (isset($_POST['line'])) {
           fwrite($logfile,"[".date("Y-m-d H:i:s")."] ".$_POST['line']);
         } else {
           echo('Non ho trovato la linea da scrivere!<br>');
         }
         fclose($logfile);
         echo('Written the new line!<br>');
       }
     } else {
       echo('Comando reset invalido!<br>');
     }

   }
   print_r(error_get_last());
 ?>
 </p>
</div>
</div>
</div>

<!-- Footer -->
<footer class="footer">
</footer>

</body>
</html>

