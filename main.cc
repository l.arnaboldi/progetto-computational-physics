

//    Convenzione sugli indici
//
//             vento ---->
//     __________________________
//    |                          |
//    |                          | y
//    |                          | i
//    |                          | M
//    |                          |
//     --------------------------
//                x j N

#include <fstream>
#include <iostream>
#include <cstdio>
#include <vector>
#include <array>
#include <cmath>
#include <string>
#include <sstream>  // stringstream
#include <utility>  // pair
#include <iomanip>  // setprecision
#include <stdlib.h> // to run shell script, rand
#include "eigenmvn.h"
#include "dtoa_milo.h"
#include "itoa-benchmark/src/sse2.cpp"
#include "ryu/ryu/d2fixed.c"

using namespace std;

// global variables
#define output_decimal_digits 4

#if DEBUG
  const unsigned int N = 1024;
  const unsigned int MAX_FRAME = 1;
  const int size_chunk = 1;
  const unsigned int FPS = 30;
#else
  const unsigned int N = 1024; // ATTENZIONE: N deve essere divisibile per 4*size_chunk!
  const unsigned int MAX_FRAME = 8000;
  const int size_chunk = 16; // size of correlated chunk
  const unsigned int FPS = 30;
#endif
const unsigned int M = N/4;
const double convergence_accuracy = 1e-3;
const unsigned int convergence_steps = 20;
const unsigned int convergence_grid_spot = 20;
const double MIN_N_tot = 0.01*convergence_accuracy*N*M; // di fatto non dovrebbe mai succedere
const double MAX_N_tot = 10 * N*M;
unsigned int T; // Number of iteration
const unsigned int matrix_size = (N+4)*(M+2);
const double Lx = 1.;
const double Ly = Lx/4.;
vector <pair<double,double>> coord; // conversione tra griglia e coordinate fisiche

//Malthus growth rate
double mean_a;
double std_a;
double corr_a;
// Competition
double b;

double cap; //capacity

// Advetionn and diffusion
double diff;
double vel;

double sigma; // varianza della condizione iniziale

const double d = Lx/double(N-1); // grid step | N-1 perchè tra N punti ci sono N-1 spazi
double dt; // time step;
const double num_char_time = 2.; // quante volte il tempo caratteristico deve durare la simulazione

// These are proportional to diff and vel in discrete lattice units
// (no point in recalculating these at every time step)
double Ddiff;
double Dvel;

const double alpha = 0.25, delta = 0.25; //CFL coefficients
const double frame_interval = 1./double(FPS); // ogni quanto tempo devo salvare un frame

/// Funzioni ausiliari
// (i,j) to linear index
inline unsigned int ij(unsigned int i, unsigned int j);
// salva su file
inline void fast_write_string(FILE *out, char *str);
void save_on_file(const string filename, const vector<pair<double, double> > &coord, const vector<double> &A);

void init_coord() {
  coord.resize(matrix_size);
  for (unsigned int i = 1; i < M+1; i++) {
    for (unsigned int j = 2; j < N+2; j++) {
      coord[ij(i,j)] = make_pair(double(j-2)*d, double(i-1)*d);
    }
  }
}

void initial_condition(vector<double> &A, double sigma) {
  A.resize(matrix_size);
  double sigma_sq = sigma * sigma;
  for (unsigned int i = 1; i < M+1; i++) {
    for (unsigned int j = 2; j < N+2; j++) {
      A[ij(i,j)] = exp(-(pow(coord[ij(i,j)].first-Lx/3., 2) +
      pow(coord[ij(i,j)].second-Ly/2., 2))/sigma_sq);
      //clog<< fixed << setprecision(15) << A[ij(i,j)] << endl;
    }
  }
}

void random_reaction(vector<double>& R, string working_dir_path) {
  int size_chunk_sq = size_chunk*size_chunk;
  Eigen::MatrixXd mean(size_chunk_sq,1);
  Eigen::MatrixXd covar(size_chunk_sq,size_chunk_sq);

  for(int i=0;i<size_chunk_sq;i++) {
    mean(i) = mean_a;
    for(int j=0;j<size_chunk_sq;j++) {
      covar(i,j) = corr_a;
    }
    covar(i,i) = std_a;
  }

  Eigen::EigenMultivariateNormal<double> normX_solver(mean,covar);

  Eigen::MatrixXd out(M*N/size_chunk_sq,size_chunk_sq);
  out = normX_solver.samples(M*N/size_chunk_sq).transpose();

  int i,j;
  for (int u=0;u<M;u++) {
    for (int v=0;v<N;v++) {
      i = u/size_chunk; j=v/size_chunk;
      R[u*N+v] = out(i*N/size_chunk+j,(u%size_chunk)*size_chunk+(v%size_chunk));
    }
  }
  /*
  * Sovrappongo anche due gaussiane per fare "isole localizzate"
  for (unsigned int i = 1; i < M+1; i++) {
  for (unsigned int j = 2; j < N+2; j++) {
  R[(i-1)*N+(j-2)] = R[(i-1)*N+(j-2)] + 0.5*exp(-(pow(coord[ij(i,j)].first-Lx/2., 2) +
  pow(coord[ij(i,j)].second-Ly/2., 2))/0.001) +
  0.5*exp(-(pow(coord[ij(i,j)].first-(Lx*4./5.), 2) +
  pow(coord[ij(i,j)].second-Ly/2., 2))/.001);
}
}
*/
}

void add_rectangles(vector<double> &R, vector<pair<array<int,4>,double>> &rect) {
  for (auto &r: rect) {
    auto &b = r.first;
    for (int i = b[2]; i < b[3] and i<M; i++) {
      for (int j = b[0]; j < b[1] and j < N; j++) {
        R[i*N+j] += r.second;
      }
    }
  }
}

void step(vector<double> &A, vector<double> &B, vector<double>& R, double &h_max) { // aggiungere parametri in base alla tecnica scelta
  // Periodic BC imposed on A
  for (int j=2;j<N+2;j++) {
    A[ij(0,j)] = A[ij(M,j)];
    A[ij(M+1,j)] = A[ij(1,j)];
  }
  for (int i=1;i<M+1;i++) {
    A[ij(i,0)] = A[ij(i,N)];
    A[ij(i,1)] = A[ij(i,N+1)];
    A[ij(i,N+2)] = A[ij(i,2)];
    A[ij(i,N+3)] = A[ij(i,3)];
  }

  // Update
  for (int i=1;i<M+1;i++) {
    for (int j=2;j<N+2;j++) {
      // Diffusion and velocity
      B[ij(i,j)] = max(0., Ddiff*A[ij(i-1,j-1)] + 4*Ddiff*A[ij(i-1,j)] + Ddiff*A[ij(i-1,j+1)]
      + (4*Ddiff+Dvel)*A[ij(i,j-1)] + (-20*Ddiff+1)*A[ij(i,j)] + (4*Ddiff-Dvel)*A[ij(i,j+1)]
      + Ddiff*A[ij(i+1,j-1)] + Ddiff*A[ij(i+1,j)] + Ddiff*A[ij(i+1,j+1)]
      // Reaction
      + dt*(R[(i-1)*N+j-2]*A[ij(i,j)] - b*A[ij(i,j)]*A[ij(i,j)]));
      h_max = max(h_max, B[ij(i,j)]);
    }
  }
}

double measure_total(vector<double> &A) {
  double res = 0.;
  for (unsigned int i = 1; i < M+1; i++) {
    for (unsigned int j = 2; j < N+2; j++) {
      res += A[ij(i,j)];
    }
  }
  return res;
}

double measure_flow_x(vector<double> &A, unsigned int x) {
  double res = 0.;
  for (unsigned int i = 1; i < M+1; i++) {
    res += -diff*(A[ij(i,x+1)]-A[ij(i,x-1)])/2. + vel * A[ij(i,x)]*d; // https://en.wikipedia.org/wiki/Convection–diffusion_equation#Derivation
  }
  return res;
}

inline bool double_convergence(double &a, double &b);

int main(int argc, char** argv) {
  if (argc < 2) {
    cerr << "File configurazione mancante!" << endl;
    return 0;
  }
  if (N%(4*size_chunk)!=0) {//nota: execute.py non catcha l'errore per ora, anche se non credo ci servirà mai di automatizzarlo.
    cerr << "Per N = "+to_string(N)+" e size_chunk = "+to_string(size_chunk)+" non riesco a creare reaction!" << endl;
    return 0;
  }
  string config_filename(argv[1]);
  string working_dir_path;
  if (argc >= 3) {
    working_dir_path= string(argv[2]);
  } else {
    working_dir_path = string("./tmp/");
  }

  // Files delle misure
  string measures_filename = working_dir_path + "measures.txt";
  FILE* measures_out = fopen(measures_filename.c_str(), "w");

  // Leggo tutto l'input
  clog << "Leggo la configurazione... ";
  clog << config_filename << endl;
  ifstream fin(config_filename);
  fin >> mean_a >> std_a >> corr_a;
  fin >> b;
  fin >> diff;
  fin >> vel;
  fin >> sigma;
  // Guardo se ho già letto tutto, se no devo prendere i rettangoli
  vector<pair<array<int,4>,double>> rectangles;
  if (fin.peek() != EOF) {
    int num_of_rect;
    fin >> num_of_rect;
    rectangles.resize(num_of_rect);
    for (auto &r: rectangles) {
      for (unsigned int i = 0; i < 4; i++)
        fin >> r.first[i];
      fin >> r.second;
    }
  }
  fin.close();
  clog << "Fatto!" << endl;

  dt = min(min(delta*d*d/diff, alpha*d/fabs(vel)), 0.1);
  // Instability if diffusion is too low
  double minDiff = 0.0000005;
  if (diff < vel*vel*dt/2.) {
    clog << "WARNING: La diffusione troppo piccola.\n Ricalcolato dt, e diff posto a " << minDiff << " (config: "<< diff << ")" << endl;
    diff = minDiff;
    dt = 2*diff/(vel*vel);
  }

  clog << "dt = " << dt << endl;
  if (dt<0.0000001) clog << "WARNING: dt probabilmente troppo piccolo" << endl;

  // Setting discrete diff and vel
  Ddiff = diff*dt/(6*d*d);
  Dvel = dt*vel/(2*d);

  vector<double> A(matrix_size), B(matrix_size); // containers for the evolution
  vector<double> reaction(M*N);

  clog << "Genero le coordinate... ";
  init_coord();
  clog << "Fatto!" << endl;

  clog << "Genero la chimica... ";
  random_reaction(reaction, working_dir_path);
  add_rectangles(reaction, rectangles);
  FILE *out_print = fopen((working_dir_path+"reaction.txt").c_str(), "w");
  char temp_string[25];
  for (int i = 0; i < M; ++i) {
    for (unsigned int j = 0; j < N; ++j) {
      d2fixed_buffered(j, output_decimal_digits, temp_string);//, output_decimal_digits);
      fast_write_string(out_print, temp_string);
      putc_unlocked(' ', out_print);
      d2fixed_buffered(i, output_decimal_digits, temp_string);//, output_decimal_digits);
      fast_write_string(out_print, temp_string);
      putc_unlocked(' ', out_print);
      d2fixed_buffered(reaction[i*N+j], output_decimal_digits, temp_string);//, output_decimal_digits);
      fast_write_string(out_print, temp_string);
      putc_unlocked('\n', out_print);
    }
  }
  fclose(out_print);
  clog << "Fatto!" << endl;

  clog << "Genero le coordinate iniziali... ";
  initial_condition(A, sigma);
  clog << "Fatto!" << endl;

  cap = mean_a/b; // setting capacity
  clog << "Capacità: " << cap << endl;

  clog << "Simulazione..." << endl;

  unsigned int frame_counter = 0;
  unsigned int iter = 0;
  char tmp_str[25]; // stringa temporanea usata per convertire i numeri
  double N_tot = (MAX_N_tot + MIN_N_tot)/2., last_N_tot; // integrale della densità di batteri
  double flow_in, flow_out, last_flow_in, last_flow_out; //flussi
  double max_height = 0.; //altezza massima, per il grafico
  double tau1 = -1.;//tempo a cui flux_out>flux_in per la prima volta. F_in a tempo 0
  double tau2 = -1.;//tempo a cui flux_out>flux_in per la prima volta. F_in simultaneo
  double flow_in_0 = measure_flow_x(A, N/3+2); //flusso iniziale al tempo 0
  unsigned int counted_convergence_steps = 0;
  while (true) {
    // Check if we have to stop
    if (N_tot < MIN_N_tot) {
      clog << "N_tot è sotto il minimo" << endl;
      break;
    } else if (N_tot>MAX_N_tot){
      clog << "N_tot è sopra il massimo" << endl;
      break;
    } else if (frame_counter>=MAX_FRAME){
      clog << "Raggiunto il massimo numero di frame" << endl;
      break;
    } else if (counted_convergence_steps > convergence_steps){
      clog << "Rilevata convergenza" << endl;
      break;
    }
    // Move
    step(A, B, reaction, max_height);

    // Save frame if needed
    if (double(iter)*dt - frame_counter * frame_interval > 0.) { //Save frame
      save_on_file(working_dir_path+"frames/frame_"+to_string(frame_counter)+".txt", coord, A);
      clog << "Saved frame: " << frame_counter << '\n';

      // Faccio le misure
      last_N_tot = N_tot;
      N_tot = measure_total(A);
      last_flow_in = flow_in;
      flow_in = measure_flow_x(A, N/3+2); //flusso a metà
      last_flow_out = flow_out;
      flow_out = measure_flow_x(A, N+1);  //flusso in fondo
      if (flow_out>flow_in_0 && tau1<0) tau1 = double(iter)*dt;
      if (flow_out>flow_in && tau2<0) tau2 = double(iter)*dt;

      // Scrivo sul file delle misure
      u32toa_sse2(frame_counter, tmp_str);
      fast_write_string(measures_out, tmp_str);
      putc_unlocked(' ', measures_out);

      dtoa_milo(N_tot, tmp_str);
      fast_write_string(measures_out, tmp_str);
      putc_unlocked(' ', measures_out);

      dtoa_milo(flow_in, tmp_str);
      fast_write_string(measures_out, tmp_str);
      putc_unlocked(' ', measures_out);

      dtoa_milo(flow_out, tmp_str);
      fast_write_string(measures_out, tmp_str);
      putc_unlocked('\n', measures_out);

      ++frame_counter;

      // Controllo se converge
      if (double_convergence(last_N_tot, N_tot) and double_convergence(last_flow_in, flow_in) and double_convergence(last_flow_out, flow_out)) {
        cout << "Convergence level: " << counted_convergence_steps << endl;
        bool is_converging = true;
        unsigned int i, x, y;;
        for (i = 0; i < convergence_grid_spot and is_converging; i++) {
          x = 2 + rand()%N;
          y = 1 + rand()%M;
          is_converging = double_convergence(A[ij(y,x)], B[ij(y,x)]);
        }
        if (is_converging) {
          ++counted_convergence_steps;
        } else {
          counted_convergence_steps = 0;
        }
      } else {
        counted_convergence_steps = 0;
      }
    }
    swap(A, B);
    iter++;
  }
  fclose(measures_out);

  ofstream program_output(working_dir_path + "program_output.txt");
  program_output << fixed << setprecision(10) << max_height << endl;
  program_output << fixed << setprecision(10) << tau1 << endl;
  program_output << fixed << setprecision(10) << tau2 << endl;
  program_output.close();

  clog << "Finito!" << endl;
}

// write on file
inline void fast_write_string(FILE *out, char *str) {
  int i = 0;
  while (str[i] != '\0') {
    putc_unlocked(str[i], out);
    ++i;
  }
}

inline bool double_convergence(double &a, double &b) {
  return (fabs(a-b)/a < convergence_accuracy);
}

void save_on_file(const string filename, const vector<pair<double,double>> &coord,const vector<double> &A) {
    // Open the file
    FILE *out = fopen(filename.c_str(), "w");
    char temp_string[25];
    for (unsigned int i = 1; i < M+1; ++i) {
      for (unsigned int j = 2; j < N+2; ++j) {
        d2fixed_buffered(coord[ij(i, j)].first, output_decimal_digits, temp_string);//, output_decimal_digits);
        fast_write_string(out, temp_string);
        putc_unlocked(' ', out);
        d2fixed_buffered(coord[ij(i, j)].second, output_decimal_digits, temp_string);//, output_decimal_digits);
        fast_write_string(out, temp_string);
        putc_unlocked(' ', out);
        d2fixed_buffered(A[ij(i, j)], output_decimal_digits, temp_string);//, output_decimal_digits);
        fast_write_string(out, temp_string);
        putc_unlocked('\n', out);
      }
    }
    fclose(out);
  }

  inline unsigned int ij(unsigned int i, unsigned int j) {
    return i*(N+4)+j;
  }
