# Computational Physics
Questo è il progetto di Bruno Bucciotti e Luca Arnaboldi prodotto durante il corso di Computational Physics del prof. Sauro Succi; a.a. 2019/2020, secondo semestre, SNS.

## ```main.cc```
Questo è il file che contiene la simulazione vera e propria, in C++.
Tutta la configurazione avviene nelle variabili prima del codice effettivo.

### Comando di compilazione
```g++ -I ./eigen-3.3.7/ -Ofast -std=gnu++17 main.cc```

Per il funzionamento di ```execute.py```l'eseguibile prodotto deve avere il nome ```a.out```.

### Parametri
L'eseguibile si aspetta come primo argomento il file di configurazione da usare per la simulazione. Come secondo argomento *opzionale* si può inserire il percorso della directory di lavoro, ovvero dove il programma salverà il file con le misure ```measure.txt```, e dove si aspetta di tovare una cartella chiamata ```frames/```, dove verranno salvati i vari frame. *NB: se nella cartella di lavoro non c'è ```frames/``` il programma va in Segmentation Fault; se si usa ```execute.py``` non c'è bisogno di creare la cartella ```frames/``` perchè viene creata in automatico. Se il secondo argomento non viene specificato di default vale ```./tmp/```.

## ```execute.py```
Questo file si occupa di lanciare in automatico le simulazioni e la creazione delle immagini. Per ogni sottocartella di ```simulations/``` viene eseguita una simulazione usando come file di configurazione ```config.txt``` che ci si aspetta di trovare all'interno di ogni sottocartella. All'interno di ogni cartella di simulazione vengono creati i seguenti files:
- ```simulation_log.txt```: contiene l'output dell'esecuzione della simulazione, che in ogni caso viene anche riporatato a video durante l'esecuzione;
- ```measures.txt```: contiene i risultai delle misure fatte ad ogni frame;
- ```frames/frame_i.txt```: sono i frame prodotti dalla simulazione. Non è necessario che la cartella ```frames``` esista prima dell'avvio.
- ```output.gif```: è la GIF che è nata dalla simulazione.

**Chiudere il programma NON chiude i processi che ha avviato; questi vanno uccisi con il comando ```kill``` per non lasciare in background programmi molto dispendiosi in termini di CPU e memoria.** A tal proposito il programma stampa anche i codici PID dei processi che ha attivato.

### Log online
Per non doversi loggare e controllare che la simulazione stia funzionando di continuo è possibile far stampare il log su una pagina web. Ecco l'elenco delle cose da fare sul webserver:
 - Mettere il file ```log.php``` su un Web Server (con PHP installato);
 - Nella stessa directory del file sul web server creare la cartella ```log```;
 - Sempre nella cartella di ```log.php```, eseguire il comando ```touch log/logfile.txt```. Serve a creare il file su cui verrà salvato il log man mano che vengono ricevuti i dati;
 - Bisogna dare i permessi all'utente ```www-data``` di scrivere sul file appena creato. Su UZ il comando giusto è ```fs setacl -dir ./log -acl www-data diklrw```. Su sistemi che non sono UZ (e che quindi non usano presumibilmente AFS) dovrebbe bastare ```chmod``` con i giusti parametri.
 Al termine di ciò basta settare la variabile ```ONLINE_LOGGING_PAGE``` all'indirizzo del file appena messo sul webserver e il gioco è fatto: basterà recarsi allo stesso indirizzo appena inserito da un browser e si può il log in tempo (quasi) reale.

## ```gnuscript```
File di GNUPlot che genera le immagini GIF. Vuole due parametri:
 - ```iter```: il numero di frames;
 - ```path```: il percorso alla cartella di lavoro. Il programma di aspetta di trovare una sottodirectory di ```path```    chiamata ```frames``` da cui prendere i frame con cui generare la GIF.
 Il risultato viene salvato nel file ```output.gif``` all'interno di ```path```.
