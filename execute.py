
import sys
import os
import subprocess
import requests
import shutil
from pathlib import Path

SIMULATION_DIR = "./simulations/"
ONLINE_LOGGING_PAGE = None

# Prendo tutte le cartelle
simulations = next(os.walk(SIMULATION_DIR))[1]

def sprint(arg, end='\n'):
    print(arg, end=end)
    if ONLINE_LOGGING_PAGE is not None:
        line = str(arg)
        # Check endline
        if not line.endswith('\n'):
            line = line + '\n'
        # Check spaces
        if line.startswith('    '):
            line = '&emsp;&emsp;' + line[4:]
        elif line.startswith('  '):
            line = '&emsp;' + line[2:]
        r = requests.post(ONLINE_LOGGING_PAGE,
                          data={'reset': 'false',
                                'line' : line,
                          }
                         )

def reset_online(soft):
    if ONLINE_LOGGING_PAGE is not None:
        if soft:
            m = 'soft'
        else:
            m = 'hard'
        r = requests.post(ONLINE_LOGGING_PAGE, data={'reset': 'true', 'reset_mode': m,})

reset_online(soft=False) # Hard reset the online log
# Load the blacklist
with open('blacklist.txt') as f:
    content = f.readlines()
blacklist = [x.strip() for x in content] # remove annoying and char
print(blacklist)
for sim in simulations:
    sprint(f'Inizio la simulazione: {sim}')
    if sim in blacklist:
        sprint(f'Blacklisted!')
        reset_online(soft=True)
        continue
    elif Path(SIMULATION_DIR+sim+'/simulation_log.txt').exists():
        sprint(f'Already done!')
        reset_online(soft=True)
        continue

    # Check if the frames directory exits
    os.makedirs(SIMULATION_DIR+sim+'/frames/', exist_ok=True)
    # Creating the logfile
    logfile = open(SIMULATION_DIR+sim+'/simulation_log.txt', 'wb')
    #Launching the simulation
    proc = subprocess.Popen(['./a.out', SIMULATION_DIR+sim+'/config.txt', SIMULATION_DIR+sim+'/'],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    sprint(f'  PID: {proc.pid}')
    # Print the result
    while True:
        line = proc.stdout.readline()
        if line == b'' and proc.poll() != None:
            break
        sprint('    '+line.decode('utf-8'), end='') # Console
        logfile.write(line) # File Log
    logfile.close()
    sprint(f'Log salvato su {SIMULATION_DIR+sim}/simulation_log.txt')

    # Produco l'immagine di reaction
    proc = subprocess.Popen(['gnuplot', '-e', f'path="{SIMULATION_DIR+sim}/"', 'gnu_reaction'])
    sprint(f'  PID: {proc.pid}')
    proc.wait()

    #Count the number of frames
    frames = 0
    for f in next(os.walk(SIMULATION_DIR+sim+'/frames/'))[2]:
        if f.startswith('frame_'):
            frames = frames + 1

    # Copio il frame finale
    shutil.copyfile(SIMULATION_DIR+sim+f'/frames/frame_{frames-1}.txt', SIMULATION_DIR+sim +'/frame_finale.txt')

    # Produce image
    prg = open(SIMULATION_DIR+sim+'/program_output.txt', 'r')
    height = float(prg.readline())
    prg.close()

    sprint(f'\nProduco le immagine: {sim}. {frames} frames')
    proc = subprocess.Popen(['gnuplot', '-e', f'iter={frames}; height={height}; path="{SIMULATION_DIR+sim}/"', 'gnuscript'],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    sprint(f'  PID: {proc.pid}')
    while True:
        line = proc.stdout.readline()
        if line == b'' and proc.poll() != None:
            break
        sprint('    '+line.decode('utf-8'), end='') # Console & online
    proc.wait() #Questo aspetta la fine del processo. Potremmo pensare di fare lavoro paralellizzato
    sprint('Rimuovo i frames...')
    shutil.rmtree(SIMULATION_DIR+sim+'/frames/')
    sprint('Finito!')
    reset_online(soft=True)

## Questo è il comando per fare l'mp4 dalla gif
## vedi la prima risposta in
## https://unix.stackexchange.com/questions/40638/how-to-do-i-convert-an-animated-gif-to-an-mp4-or-mv4-on-the-command-line

#ffmpeg -i output.gif -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" video.mp4
